# Recovery System
This repository contains every file associated with the rocket's recoverynt system.  

## License  
The project is licensed under the [CERN OHL license](https://gitlab.com/librespacefoundation/cronos-rocket/mechanical-design/-/blob/master/LICENSE)